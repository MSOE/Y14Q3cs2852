package wk9;
/**
 * Created by Dr. Durant on 5/7/2014.  CS2852 W9-D3 example for deep/shallow copy / cloning
 */
public class TestClone {
    public static void main(String[] args) throws CloneNotSupportedException {
        Calendar c1 = new Calendar("c1");
        c1.add(new Meeting("Advising",15));
        c1.add(new Meeting("CS2852",50));
        Calendar c2 = c1.clone();
        c2.setName("c2");
        c1.remove(0);
        c1.get(0).setTitle("CS2852-21 personal notes");

        System.out.println("c1:" + c1);
        System.out.println("c2:" + c2);
    }
}
