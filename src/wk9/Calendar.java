package wk9;

import java.util.ArrayList;
import java.util.List;

/**
 * Based on code from Dr. Durant
 */
public class Calendar implements Cloneable {
    private String name;
    private List<Meeting> meetings = new ArrayList<>();

    public Calendar(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void add(Meeting meeting) {
        meetings.add(meeting);
    }

    public void remove(int i) {
        meetings.remove(i);
    }

    @Override
    public String toString() {
        StringBuffer string = new StringBuffer(name + ": ");
        for (Meeting meeting : meetings) {
            string.append("\n" + meeting);
        }
        return string.toString();
    }

    public Meeting get(int i) {
        return meetings.get(i);
    }

    @Override
    public Calendar clone() throws CloneNotSupportedException {
        Calendar copy = (Calendar)super.clone();
        copy.meetings = new ArrayList<>();
        for (Meeting meeting : this.meetings) {
            copy.meetings.add(meeting.clone());
        }
        return copy;
    }
}
