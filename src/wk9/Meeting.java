package wk9;
/**
 * Based on code from Dr. Durant
 */
public class Meeting implements Cloneable {
    private String title;
    private int minutes;

    public Meeting(String title, int minutes) {
        this.title = title;
        this.minutes = minutes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title + ": " + minutes + " minutes";
    }

    @Override
    public Meeting clone() throws CloneNotSupportedException
    {
        return (Meeting)super.clone();
    }
}











    /*
    @Override
    public Meeting clone() throws CloneNotSupportedException
    {
        return (Meeting)super.clone();
    }
    */

