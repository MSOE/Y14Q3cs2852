package wk6;

import java.util.ArrayList;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        BST<Integer> tree = new BST<>();
        for(int i=0; i<20; ++i) {
            tree.add(i);
        }
        BST<Integer> copy = tree.getCopy();
        copy.printInOrder();
    }





















    public static void main2(String[] args) {
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<10; i+=2) {
            list.add(i);
        }
        System.out.println(binarySearch(list, -1));
        System.out.println(binarySearch(list, 2));
        System.out.println(binarySearch(list, 0));
        System.out.println(binarySearch(list, null));
    }

    public static <E extends Comparable<E>> boolean binarySearch(List<E> list, E target){
        return binarySearch(list, target, 0, list.size());
    }

    public static <E extends Comparable<E>> boolean binarySearch(List<E> list, E target, int start, int end){
        boolean found = false;
        if(start!=end) {
            int index = (start+end)/2;
            E element = list.get(index);
            int comparison = element.compareTo(target);
            if(comparison==0) {
                found = true;
            } else if(comparison<0) {
                found = binarySearch(list, target, index+1, end);
            } else {
                found = binarySearch(list, target, start, index);
            }
        }
        return found;
    }
}






