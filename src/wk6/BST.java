package wk6;

public class BST<E extends Comparable<E>> {
    private Node root;
    private static class Node<E> {
        private final E value;
        private Node parent;
        private Node lKid;
        private Node rKid;

        private Node(E value) {
            this(value, null);
        }

        /**
         * Constructor
         * @param value Value to store in the collection
         * @param parent The node's parent
         */
        public Node(E value, Node parent) {
            this(value, parent, null, null);
        }

        public Node(E value, Node parent, Node lKid, Node rKid) {
            this.value = value;
            this.parent = parent;
            this.lKid = lKid;
            this.rKid = rKid;
        }

        public boolean hasLeftKid() {
            return lKid!=null;
        }
    }

    public BST() {
        root = null;
    }

    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        return subroot==null ? 0 : 1 + size(subroot.lKid) + size(subroot.rKid);
    }

    public int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        return subroot==null ? 0 :
                1 + Math.max(height(subroot.lKid), height(subroot.rKid));
    }

    public boolean contains(E target) {
        return contains(root, target);
    }

    private boolean contains(Node<E> subroot, E target) {
        boolean found = true;
        if(subroot==null) {
            found = false;
        } else {
            int comparison = subroot.value.compareTo(target);
            if(comparison<0) {
                found = contains(subroot.rKid, target);
            } else if(comparison>0) {
                found = contains(subroot.lKid, target);
            }
        }
        return found;
    }

    public boolean add(E element) {
        if(element==null) {
            throw new NullPointerException("We don't serve nulls here");
        }
        boolean added = true;
        if(root==null) {
            root = new Node<>(element);
        } else {
            added = add(root, element);
        }
        return added;
    }

    public int numberOfLeaves() {
        return numberOfLeaves(root);
    }

    // TODO
    private int numberOfLeaves(Node<E> node) {
        return 0;
    }

    private Node<E> rightRotate(Node<E> y) {
        if(y.lKid==null) {
            throw new IllegalArgumentException("Cannot right rotate if node does not have a left child");
        }
        Node<E> x = y.lKid;
        Node<E> B = x.rKid;
        Node<E> parent = y.parent;
        x.rKid = y;
        if(B!=null) {
            B.parent = y;
        }
        y.lKid = B;
        y.parent = x;
        x.parent = parent;
        if(parent==null) {
            root = x;
        } else if(parent.lKid==y) {
            parent.lKid = x;
        } else {
            parent.rKid = x;
        }
        return x;
    }

    private Node<E> leftRotate(Node<E> x) {
        if(x.rKid==null) {
            throw new IllegalArgumentException("Cannot left rotate if node does not have a right child");
        }
        Node<E> y = x.rKid;
        Node<E> B = y.lKid;
        Node<E> parent = x.parent;
        y.lKid = x;
        if(B!=null) {
            B.parent = x;
        }
        x.rKid = B;
        x.parent = y;
        y.parent = parent;
        if(parent==null) {
            root = y;
        } else if(parent.lKid==x) {
            parent.lKid = y;
        } else {
            parent.rKid = y;
        }
        return y;
    }


    private boolean add(Node<E> node, E element) {
        boolean added = false;
        int comparison = node.value.compareTo(element);
        if(comparison>0) {
            if(node.lKid==null) {
                node.lKid = new Node<>(element, node);
                added = true;
            } else {
                added = add(node.lKid, element);
            }
        } else if(comparison<0) {
            if(node.rKid==null) {
                node.rKid = new Node<>(element, node);
                added = true;
            } else {
                added = add(node.rKid, element);
            }
        }
        return added;
    }

    public boolean addNonRec(E element) {
        if(element==null) {
            throw new NullPointerException("We don't serve nulls here");
        }
        boolean changed = !contains(element);
        if(changed) {
            if(root==null) {
                root = new Node<>(element);
            } else {
                Node<E> walker = root;
                while(walker!=null) {
                    int comparison = walker.value.compareTo(element);
                    if(comparison<0) {
                        if(walker.rKid==null) {
                            walker.rKid = new Node<>(element, walker);
                            walker = null;
                        } else {
                            walker = walker.rKid;
                        }
                    } else {
                        if(walker.lKid==null) {
                            walker.lKid = new Node<>(element, walker);
                            walker = null;
                        } else {
                            walker = walker.lKid;
                        }
                    }
                }
            }
        }
        return changed;
    }

    public BST<E> getCopy() {
        BST<E> newGuy = new BST<>();
        getCopy(root, newGuy);
        return newGuy;
    }

    private void getCopy(Node<E> node, BST<E> tree) {
        if(node!=null) {
            getCopy(node.lKid, tree);
            getCopy(node.rKid, tree);
            tree.add(node.value);
        }
    }

    public void printInOrder() {
        printInOrder(root);
    }

    private void printInOrder(Node<E> subroot) {
        if(subroot!=null) {
            printInOrder(subroot.lKid);
            System.out.println(subroot.value);
            printInOrder(subroot.rKid);
        }
    }

    public void printPreOrder() {
        printPreOrder(root);
    }

    private void printPreOrder(Node<E> subroot) {
        if(subroot!=null) {
            System.out.println(subroot.value);
            printPreOrder(subroot.lKid);
            printPreOrder(subroot.rKid);
        }
    }

    public void printPostOrder() {
        printPostOrder(root);
    }

    private void printPostOrder(Node<E> subroot) {
        if(subroot!=null) {
            printPostOrder(subroot.lKid);
            printPostOrder(subroot.rKid);
            System.out.println(subroot.value);
        }
    }

}











