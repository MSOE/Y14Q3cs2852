package wk2;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<E> implements List<E> {

    private class Node {
        private E value;
        private Node next;

        private Node(E value, Node next) {
            this.value = value;
            this.next = next;
        }
        private Node(E value) {
            this(value, null);
        }
    }

    private Node head;
    private Node tail;

    public LinkedList() {
        head = null;
        tail = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node walker = head;
        while(walker!=null) {
            ++count;
            walker = walker.next;
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o)>=0;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        E[] newArray = (E[])new Object[size()];
        Node walker = head; //Starts the walker at the beginning of the LinkedList
        for(int i = 0; i < newArray.length; i++){
            newArray[i] = walker.value; //Adds the value at the current spot of the walker to the Array
            walker = walker.next; //Moves the walker to the next node if available
        }
        return newArray;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        Node newGuy = new Node(e);
        if(tail!=null) {
            tail.next = newGuy;
            tail = newGuy;
        } else {
            head = tail = newGuy;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if(index>=0) {
            remove(index);
        }
        return index>=0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        head = tail = null;
    }

    public E get(int index) {
        //Caches size to maximize performance
        int size = size();

        //Checks for out-of-bounds cases
        if(index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        E value = head.value;
        //Cycles through list until appropriate value is reached
        if(index!=0) {
             value = getOneBeforeNode(index+1).value;
        }
        return value;
    }

    @Override
    public E set(int index, E element) {
        indexBoundsCheck(index);
        Node node = getOneBeforeNode(index+1);
        E oldValue = node.value;
        node.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        indexBoundsCheck(index);
        int size = size();
        if (index == 0){ // add to beginning
            Node newOne = new Node(element, head);
            head = newOne;
            if(size==0) { // if started empty
                tail = newOne;
            }
        }else if (index == size){ // add to end
            Node newOne = new Node(element, null);
            tail.next = newOne;
            tail = newOne;
        }else{
            Node walker = getOneBeforeNode(index);
            walker.next = new Node(element, walker.next);
        }
    }

    private void indexBoundsCheck(int index) {
        int size = size();
        if (index < 0 || index > size){
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size);
        }
    }

    private Node getOneBeforeNode(int index) {
        Node walker = head;
        for(int i = 0; i<index-1; i++) {
            walker = walker.next;
        }
        return walker;
    }

    @Override
    public E remove(int index) {
        indexBoundsCheck(index);
        E value;
        if(index == 0) {
            value = head.value;
            head = head.next;
        } else {
            Node walker = getOneBeforeNode(index);
            value = walker.next.value;
            walker.next = walker.next.next;
            if(walker.next==null) {
                tail = walker;
            }
        }
        return value;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        boolean wasFound = false;
        for(Node walker = head; !wasFound && walker!=null; walker=walker.next) {
            ++index;
            if(!(walker.value==null && o!=null) && (walker.value==o || walker.value.equals(o))) {
                wasFound = true;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        indexBoundsCheck(fromIndex);
        indexBoundsCheck(toIndex);
        List<E> sublist = new LinkedList<>();
        Node walker = getOneBeforeNode(fromIndex+1);
        while(fromIndex<toIndex) {
            sublist.add(walker.value);
            ++fromIndex;
        }
        return sublist;
    }
}
