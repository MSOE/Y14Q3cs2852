package wk1;

import java.util.*;

public class ArrayList<E> implements List<E> {
    private Object[] data;

    private class ArrayListIterator<E> implements Iterator<E> {
        private int position = -1;

        @Override
        public boolean hasNext() {
            return position+1<size();
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("No elements left");
            }
            return (E)data[++position];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Too lazy");
            // HOMEWORK
        }
    }

    public ArrayList() {
        data = new Object[0];
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public boolean isEmpty() {
        return data.length==0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o)>=0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator<>();
    }

    @Override
    public Object[] toArray() {
        Object[] copy = new Object[size()];
        System.arraycopy(data, 0, copy, 0, size());
        return copy;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public boolean add(E e) {
        // Create a new array (one bigger than current)
        Object[] temp = new Object[size()+1];
        // Copy all values from old to new
        for(int i=0; i<size(); ++i) {
            temp[i] = data[i];
        }
        // Put e in last position of new array
        temp[size()] = e;
        // Connect attribute reference to new array
        data = temp;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean changed = false;
        for(int i=0; !changed && i < size(); ++i){
            if(o==data[i] || o.equals(data[i])){
                remove(i);
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public void clear() {
        data = new Object[0];
    }

    @Override
    public E get(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException(index + " Size: " + size());
        }
        return (E)data[index];
    }

    @Override
    public E set(int index, E element) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException(index + " Size: " + size());
        }
        E oldValue = (E)data[index];
        data[index] = element;
        return oldValue;
    }

    @Override
    public void add(int index, E e) {
        if(index < 0 || index >= size())
            throw new IndexOutOfBoundsException(index + " Size: " +size());

        Object[] newData = new Object[size()+1];

        System.arraycopy(data, 0, newData, 0, index);
        newData[index] = e;
        System.arraycopy(data, index, newData, index+1, size() - index);

        data = newData;
    }

    @Override
    public E remove(int index) {
        E removedValue = null;
        // check for conditions specified in API for index
        //  and throw the proper exception if needed
        if (index < 0 || index >= size()){
            throw new IndexOutOfBoundsException(index + " Size: " + size());
        }else{
            removedValue = (E)data[index];
            // new array 1 smaller (removing an object)
            Object[] newData = new Object[size()-1];
            // add objects to new array
            int x = 0;
            for (int i=0; i<size()-1; i++, x++) {
                if (i != index){
                    // if not removing, move from old to new arrays
                    newData[i] = data[x];
                }else{
                    x++; // need to skip the index in data[]
                    newData[i] = data[x];
                }
            }
            data = newData;
        }
        return removedValue;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        for(int i=0; index==-1 && i<size(); ++i) {
            if(o==data[i] || data[i].equals(o)) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("We don't do this stuff.");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        if((fromIndex<0 || fromIndex>=size()) && (toIndex<0 || toIndex>=size())) {
            throw new IndexOutOfBoundsException(fromIndex + " to " + toIndex + " with size: " + size());
        }
        List<E> sublist = new ArrayList<>();
        for(int i=fromIndex; i<toIndex; ++i) {
            sublist.add((E)data[i]);
        }
        return sublist;
    }
}
