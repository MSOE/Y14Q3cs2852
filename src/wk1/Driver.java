package wk1;

import wk2.LinkedList;

import javax.swing.*;
import java.util.Iterator;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("First");
        list.add("Second");
        list.add(null);
        // Standard for loop
        for(int i=0; i<list.size(); ++i) {
            System.out.println(list.get(i));
        }
        // Enhanced for loop
        for(String word : list) {
            System.out.println(word);
        }
        // What the enhanced for loop is actually doing under the hood
        {
            Iterator<String> itr = list.iterator();
            while(itr.hasNext()) {
                String word = itr.next();
                System.out.println(word);
            }
        }
    }
}
