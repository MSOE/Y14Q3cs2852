package wk8;

public class Rope {
    private String word;

    public Rope(String word) {
        if(word==null) {
            throw new NullPointerException("No null ropes allowed");
        }
        this.word = word;
    }

    @Override
    public int hashCode() {
        int code = 0;
        for(int i=0; i<word.length(); ++i) {
            code += (word.charAt(i) - 'a')*i;
        }
        return code;
    }

    public boolean equalsIgnoreCase(String anotherWord) {
        return word.equalsIgnoreCase(anotherWord);
    }

    @Override
    public String toString() {
        return word;
    }
}
