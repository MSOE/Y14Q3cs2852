package wk8;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            Rope word = new Rope("");
            while(!word.equalsIgnoreCase("quit")) {
                word = new Rope(in.next());
                System.out.println(word + ": " + word.hashCode());
            }
        }
    }
}
