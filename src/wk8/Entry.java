package wk8;

public class Entry<K, V> {
    private K key;
    private V value;

    public Entry(K key, V value) {
        if(key==null) {
            throw new NullPointerException("No null keys please");
        }
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object that) {
        if(that instanceof Entry) {
            return key.equals(((Entry<K, V>)that).key);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    public static void main(String[] args) {
        Entry<Integer, String> first = new Entry<>(1, "one");
        Entry<Integer, String> second = new Entry<>(1, "January");
        System.out.println(first.hashCode());
        System.out.println(second.hashCode());
        if(!first.equals(second)) {
            System.err.println("You blew it");
        }
    }
}

