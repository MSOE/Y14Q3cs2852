package wk5;

public class Driver {
    public static void main(String[] args) {
        PureQueue<String> queue = new Queue<>(3);
        queue.offer("The");
        queue.offer("reign");
        queue.offer("in");
        queue.offer("Spain");
        queue.offer("stays");
        queue.offer("mainly");
        queue.offer("in");
        queue.offer("the");
        queue.offer("plane");
        while(queue.peek()!=null) {
            System.out.println(queue.poll());
        }
    }
}
