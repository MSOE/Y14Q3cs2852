package wk5;

import java.util.LinkedList;
import java.util.List;

public class Queue<E> implements PureQueue<E> {
    private final int maxSize;
    private List<E> data = new LinkedList<E>();

    public Queue(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean offer(E e) {
        boolean added = false;
        if(maxSize==-1 || data.size()<maxSize) {
            data.add(e);
            added = true;
        }
        return added;
    }

    @Override
    public E poll() {
        E removed = null;
        if(!data.isEmpty()) {
            removed = data.remove(0);
        }
        return removed;
        // return data.isEmpty() ? null : data.remove(0);
    }

    @Override
    public E peek() {
        return data.isEmpty() ? null : data.get(0);
    }
}
