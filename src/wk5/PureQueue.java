package wk5;

public interface PureQueue<E> {
    public boolean offer(E e);
    public E poll();
    public E peek();
}
